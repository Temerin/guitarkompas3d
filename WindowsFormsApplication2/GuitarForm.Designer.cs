﻿namespace WindowsFormsApplication2
{
    partial class Form1
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.checkBoxCut = new System.Windows.Forms.CheckBox();
            this.comboBoxType = new System.Windows.Forms.ComboBox();
            this.numericFret = new System.Windows.Forms.NumericUpDown();
            this.comboBoxLowDeck = new System.Windows.Forms.ComboBox();
            this.comboBoxUpDeck = new System.Windows.Forms.ComboBox();
            this.comboBoxShell = new System.Windows.Forms.ComboBox();
            this.comboBoxFretboard = new System.Windows.Forms.ComboBox();
            this.comboBoxTuners = new System.Windows.Forms.ComboBox();
            this.comboBoxBridge = new System.Windows.Forms.ComboBox();
            this.comboBoxMarkers = new System.Windows.Forms.ComboBox();
            this.comboBoxHeadstock = new System.Windows.Forms.ComboBox();
            this.checkBoxFlag = new System.Windows.Forms.CheckBox();
            this.numericOutlet = new System.Windows.Forms.NumericUpDown();
            this.labelFret = new System.Windows.Forms.Label();
            this.labelOutlet = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.buttonBuild = new System.Windows.Forms.Button();
            this.buttonKompStart = new System.Windows.Forms.Button();
            this.buttonClear = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.numericFret)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericOutlet)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // checkBoxCut
            // 
            this.checkBoxCut.AutoSize = true;
            this.checkBoxCut.Location = new System.Drawing.Point(169, 61);
            this.checkBoxCut.Name = "checkBoxCut";
            this.checkBoxCut.Size = new System.Drawing.Size(59, 17);
            this.checkBoxCut.TabIndex = 0;
            this.checkBoxCut.Text = "Вырез";
            this.checkBoxCut.UseVisualStyleBackColor = true;
            this.checkBoxCut.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // comboBoxType
            // 
            this.comboBoxType.FormattingEnabled = true;
            this.comboBoxType.Items.AddRange(new object[] {
            "Классика",
            "Вестерн"});
            this.comboBoxType.Location = new System.Drawing.Point(78, 18);
            this.comboBoxType.Name = "comboBoxType";
            this.comboBoxType.Size = new System.Drawing.Size(121, 21);
            this.comboBoxType.TabIndex = 1;
            this.comboBoxType.Text = "Классика";
            // 
            // numericFret
            // 
            this.numericFret.Location = new System.Drawing.Point(109, 59);
            this.numericFret.Maximum = new decimal(new int[] {
            25,
            0,
            0,
            0});
            this.numericFret.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericFret.Name = "numericFret";
            this.numericFret.Size = new System.Drawing.Size(35, 20);
            this.numericFret.TabIndex = 2;
            this.numericFret.Value = new decimal(new int[] {
            16,
            0,
            0,
            0});
            // 
            // comboBoxLowDeck
            // 
            this.comboBoxLowDeck.FormattingEnabled = true;
            this.comboBoxLowDeck.Items.AddRange(new object[] {
            "Кедр",
            "Клен",
            "Липа",
            "Сосна"});
            this.comboBoxLowDeck.Location = new System.Drawing.Point(144, 154);
            this.comboBoxLowDeck.Name = "comboBoxLowDeck";
            this.comboBoxLowDeck.Size = new System.Drawing.Size(111, 21);
            this.comboBoxLowDeck.TabIndex = 3;
            this.comboBoxLowDeck.Text = "Кедр";
            // 
            // comboBoxUpDeck
            // 
            this.comboBoxUpDeck.FormattingEnabled = true;
            this.comboBoxUpDeck.Items.AddRange(new object[] {
            "Кедр",
            "Клен",
            "Липа",
            "Сосна"});
            this.comboBoxUpDeck.Location = new System.Drawing.Point(144, 181);
            this.comboBoxUpDeck.Name = "comboBoxUpDeck";
            this.comboBoxUpDeck.Size = new System.Drawing.Size(111, 21);
            this.comboBoxUpDeck.TabIndex = 4;
            this.comboBoxUpDeck.Text = "Кедр";
            this.comboBoxUpDeck.SelectedIndexChanged += new System.EventHandler(this.comboBoxUpDeck_SelectedIndexChanged);
            // 
            // comboBoxShell
            // 
            this.comboBoxShell.FormattingEnabled = true;
            this.comboBoxShell.Items.AddRange(new object[] {
            "Кедр",
            "Клен",
            "Липа",
            "Сосна"});
            this.comboBoxShell.Location = new System.Drawing.Point(144, 208);
            this.comboBoxShell.Name = "comboBoxShell";
            this.comboBoxShell.Size = new System.Drawing.Size(111, 21);
            this.comboBoxShell.TabIndex = 5;
            this.comboBoxShell.Text = "Кедр";
            // 
            // comboBoxFretboard
            // 
            this.comboBoxFretboard.FormattingEnabled = true;
            this.comboBoxFretboard.Items.AddRange(new object[] {
            "Клен",
            "Красное дерево",
            "Полисандр"});
            this.comboBoxFretboard.Location = new System.Drawing.Point(144, 127);
            this.comboBoxFretboard.Name = "comboBoxFretboard";
            this.comboBoxFretboard.Size = new System.Drawing.Size(111, 21);
            this.comboBoxFretboard.TabIndex = 6;
            this.comboBoxFretboard.Text = "Клен";
            // 
            // comboBoxTuners
            // 
            this.comboBoxTuners.FormattingEnabled = true;
            this.comboBoxTuners.Items.AddRange(new object[] {
            "Классические",
            "Альтернативный вариант"});
            this.comboBoxTuners.Location = new System.Drawing.Point(144, 235);
            this.comboBoxTuners.Name = "comboBoxTuners";
            this.comboBoxTuners.Size = new System.Drawing.Size(111, 21);
            this.comboBoxTuners.TabIndex = 7;
            this.comboBoxTuners.Text = "Классические";
            // 
            // comboBoxBridge
            // 
            this.comboBoxBridge.FormattingEnabled = true;
            this.comboBoxBridge.Items.AddRange(new object[] {
            "Классический",
            "С зажимами "});
            this.comboBoxBridge.Location = new System.Drawing.Point(144, 262);
            this.comboBoxBridge.Name = "comboBoxBridge";
            this.comboBoxBridge.Size = new System.Drawing.Size(111, 21);
            this.comboBoxBridge.TabIndex = 8;
            this.comboBoxBridge.Text = "Классический";
            // 
            // comboBoxMarkers
            // 
            this.comboBoxMarkers.FormattingEnabled = true;
            this.comboBoxMarkers.Items.AddRange(new object[] {
            "Сверху и у струн",
            "Только сверху",
            "Только у струн"});
            this.comboBoxMarkers.Location = new System.Drawing.Point(144, 289);
            this.comboBoxMarkers.Name = "comboBoxMarkers";
            this.comboBoxMarkers.Size = new System.Drawing.Size(111, 21);
            this.comboBoxMarkers.TabIndex = 9;
            this.comboBoxMarkers.Text = "Сверху и у струн";
            // 
            // comboBoxHeadstock
            // 
            this.comboBoxHeadstock.FormattingEnabled = true;
            this.comboBoxHeadstock.Items.AddRange(new object[] {
            "Классическая",
            "Альтернативный вариант"});
            this.comboBoxHeadstock.Location = new System.Drawing.Point(144, 316);
            this.comboBoxHeadstock.Name = "comboBoxHeadstock";
            this.comboBoxHeadstock.Size = new System.Drawing.Size(111, 21);
            this.comboBoxHeadstock.TabIndex = 10;
            this.comboBoxHeadstock.Text = "Классическая";
            // 
            // checkBoxFlag
            // 
            this.checkBoxFlag.AutoSize = true;
            this.checkBoxFlag.Location = new System.Drawing.Point(169, 87);
            this.checkBoxFlag.Name = "checkBoxFlag";
            this.checkBoxFlag.Size = new System.Drawing.Size(69, 17);
            this.checkBoxFlag.TabIndex = 11;
            this.checkBoxFlag.Text = "Флажок";
            this.checkBoxFlag.UseVisualStyleBackColor = true;
            // 
            // numericOutlet
            // 
            this.numericOutlet.Location = new System.Drawing.Point(109, 85);
            this.numericOutlet.Maximum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.numericOutlet.Minimum = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.numericOutlet.Name = "numericOutlet";
            this.numericOutlet.Size = new System.Drawing.Size(35, 20);
            this.numericOutlet.TabIndex = 12;
            this.numericOutlet.ThousandsSeparator = true;
            this.numericOutlet.Value = new decimal(new int[] {
            25,
            0,
            0,
            0});
            // 
            // labelFret
            // 
            this.labelFret.AutoSize = true;
            this.labelFret.Location = new System.Drawing.Point(6, 61);
            this.labelFret.Name = "labelFret";
            this.labelFret.Size = new System.Drawing.Size(99, 13);
            this.labelFret.TabIndex = 13;
            this.labelFret.Text = "Количество ладов";
            // 
            // labelOutlet
            // 
            this.labelOutlet.AutoSize = true;
            this.labelOutlet.Location = new System.Drawing.Point(6, 87);
            this.labelOutlet.Name = "labelOutlet";
            this.labelOutlet.Size = new System.Drawing.Size(97, 13);
            this.labelOutlet.TabIndex = 14;
            this.labelOutlet.Text = "Диаметр розетки";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(4, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 13);
            this.label2.TabIndex = 17;
            this.label2.Text = "Тип гитары";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 130);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(91, 13);
            this.label3.TabIndex = 18;
            this.label3.Text = "Материал грифа";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 184);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(128, 13);
            this.label4.TabIndex = 19;
            this.label4.Text = "Материал верхней деки";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 157);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(125, 13);
            this.label5.TabIndex = 20;
            this.label5.Text = "Материал нижней деки";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 211);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(113, 13);
            this.label6.TabIndex = 21;
            this.label6.Text = "Материал оберчайки";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 238);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(83, 13);
            this.label7.TabIndex = 22;
            this.label7.Text = "Форма колков";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 292);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(98, 13);
            this.label8.TabIndex = 23;
            this.label8.Text = "Отметки на ладах";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 319);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(122, 13);
            this.label9.TabIndex = 24;
            this.label9.Text = "Форма головки грифа";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 265);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(67, 13);
            this.label10.TabIndex = 25;
            this.label10.Text = "Тип бриджа";
            // 
            // buttonBuild
            // 
            this.buttonBuild.Location = new System.Drawing.Point(9, 50);
            this.buttonBuild.Name = "buttonBuild";
            this.buttonBuild.Size = new System.Drawing.Size(246, 23);
            this.buttonBuild.TabIndex = 0;
            this.buttonBuild.Text = "Построить модель";
            this.buttonBuild.UseVisualStyleBackColor = true;
            this.buttonBuild.Click += new System.EventHandler(this.buttonBuild_Click);
            // 
            // buttonKompStart
            // 
            this.buttonKompStart.Location = new System.Drawing.Point(9, 21);
            this.buttonKompStart.Name = "buttonKompStart";
            this.buttonKompStart.Size = new System.Drawing.Size(246, 23);
            this.buttonKompStart.TabIndex = 1;
            this.buttonKompStart.Text = "Запустить Компас 3D";
            this.buttonKompStart.UseVisualStyleBackColor = true;
            this.buttonKompStart.Click += new System.EventHandler(this.buttonSWStart_Click);
            // 
            // buttonClear
            // 
            this.buttonClear.Location = new System.Drawing.Point(9, 79);
            this.buttonClear.Name = "buttonClear";
            this.buttonClear.Size = new System.Drawing.Size(246, 23);
            this.buttonClear.TabIndex = 2;
            this.buttonClear.Text = "Очистить\r\nданные";
            this.buttonClear.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.buttonBuild);
            this.groupBox1.Controls.Add(this.buttonClear);
            this.groupBox1.Controls.Add(this.buttonKompStart);
            this.groupBox1.Location = new System.Drawing.Point(21, 369);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(265, 112);
            this.groupBox1.TabIndex = 26;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Управление";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.checkBoxCut);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.comboBoxType);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.numericFret);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.comboBoxLowDeck);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.comboBoxUpDeck);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.comboBoxShell);
            this.groupBox2.Controls.Add(this.comboBoxFretboard);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.comboBoxTuners);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.comboBoxBridge);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.comboBoxMarkers);
            this.groupBox2.Controls.Add(this.labelOutlet);
            this.groupBox2.Controls.Add(this.comboBoxHeadstock);
            this.groupBox2.Controls.Add(this.labelFret);
            this.groupBox2.Controls.Add(this.checkBoxFlag);
            this.groupBox2.Controls.Add(this.numericOutlet);
            this.groupBox2.Location = new System.Drawing.Point(21, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(265, 351);
            this.groupBox2.TabIndex = 27;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Параметры";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(310, 489);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "Конструктор гитары";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numericFret)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericOutlet)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.CheckBox checkBoxCut;
        private System.Windows.Forms.ComboBox comboBoxType;
        private System.Windows.Forms.NumericUpDown numericFret;
        private System.Windows.Forms.ComboBox comboBoxLowDeck;
        private System.Windows.Forms.ComboBox comboBoxUpDeck;
        private System.Windows.Forms.ComboBox comboBoxShell;
        private System.Windows.Forms.ComboBox comboBoxFretboard;
        private System.Windows.Forms.ComboBox comboBoxTuners;
        private System.Windows.Forms.ComboBox comboBoxBridge;
        private System.Windows.Forms.ComboBox comboBoxMarkers;
        private System.Windows.Forms.ComboBox comboBoxHeadstock;
        private System.Windows.Forms.CheckBox checkBoxFlag;
        private System.Windows.Forms.NumericUpDown numericOutlet;
        private System.Windows.Forms.Label labelFret;
        private System.Windows.Forms.Label labelOutlet;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button buttonBuild;
        private System.Windows.Forms.Button buttonKompStart;
        private System.Windows.Forms.Button buttonClear;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
    }
}

