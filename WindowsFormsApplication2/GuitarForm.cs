﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ClassLibrary1;

namespace WindowsFormsApplication2
{
    public partial class Form1 : Form
    {
        private GuitarBuilder _guitarBuilder = new GuitarBuilder();

        private Form Form;


        public Form1()
        {
            InitializeComponent();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void labelControl_Click(object sender, EventArgs e)
        {

        }

        private void buttonSWStart_Click(object sender, EventArgs e)
        {
            try
            {
                _guitarBuilder.GuitarKsObject.OpenKompas3D();
            }
            catch (Exception)
            {
                _guitarBuilder.GuitarKsObject._kompasObject = null;
                _guitarBuilder.GuitarKsObject._ksDocumentObject = null;
                _guitarBuilder.GuitarKsObject.OpenKompas3D();
            }
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void comboBoxUpDeck_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void buttonBuild_Click(object sender, EventArgs e)
        {
            // Запись введенных данных.
            _guitarBuilder.GuitarDataObject.Record(0, false, false, 0, 20,
            0, 0, 0, 12,
            0, 0, 0, 0);
            _guitarBuilder.GuitarKsObject.CreateDocument();
            _guitarBuilder.BuildGuitar();
        }
    }
}
