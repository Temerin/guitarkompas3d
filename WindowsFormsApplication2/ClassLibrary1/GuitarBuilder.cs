﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1
{
    public class GuitarBuilder
    {

        #region Поля класса.


        /// <summary>
        /// Указатель на данные о гитаре.
        /// </summary>
        public GuitarDescription GuitarDataObject
            = new GuitarDescription(); //Необходимо реализовать передачу данных классу.

        /// <summary>
        /// Указатель на документ КОМПАС-3D.
        /// </summary>
        public Connector GuitarKsObject = new Connector();

        /// <summary>
        /// Модель тела гитары.
        /// </summary>
        BodyGuitarBuilder BodyObject = new BodyGuitarBuilder();

        /// <summary>
        /// Модель грифа гитары.
        /// </summary>
        FretboardGuitarBuilder FretboardObject = new FretboardGuitarBuilder();

        /// <summary>
        /// Модель головки грифа гитаы.
        /// </summary>
        HeadstockGuitarBuilder HeadstockObject = new HeadstockGuitarBuilder();

        #endregion

        public void BuildGuitar()
        {
            BodyObject.Build(GuitarKsObject._ksDocumentObject,
                GuitarDataObject);
            /*FretboardObject.Build(GuitarKsObject._ksDocumentObject,
                GuitarDataObject);
            HeadstockObject.Build(GuitarKsObject._ksDocumentObject,
                GuitarDataObject);*/
        }
    }
}
