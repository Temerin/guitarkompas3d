﻿namespace ClassLibrary1
{
    // Перечисление типов клавиатуры.
    public enum Markers
    {
        Atop = 0,         	// Маркеры сверху грифа
        Strings,			// Маркеры в районе стругн
		TopStrings			// Сверху и на струнах
    }
}