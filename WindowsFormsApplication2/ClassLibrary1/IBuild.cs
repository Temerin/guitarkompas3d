﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kompas6API5;

namespace ClassLibrary1
{
    interface IBuild
    {
        //public ksPart; //Не ясно. Должен быть параметр? Не в интерфейсе?
        void Build(ksDocument3D document3D, GuitarDescription data); //public abstract?
    }
}
