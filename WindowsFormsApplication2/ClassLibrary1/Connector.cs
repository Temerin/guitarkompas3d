﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kompas6API5;

namespace ClassLibrary1
{
    public class Connector
    {
        public KompasObject _kompasObject;
        public ksDocument3D _ksDocumentObject;

        public void OpenKompas3D()
        {
            if (_kompasObject == null)
            {
                var type = Type.GetTypeFromProgID("KOMPAS.Application.5");
                _kompasObject = (KompasObject)Activator.CreateInstance(type);
            }
            if (_kompasObject != null)
            {
                try
                {
                    _kompasObject.Visible = true;
                    _kompasObject.ActivateControllerAPI();
                }
                catch (Exception)
                {
                    throw new Exception();
                }
            }
        }

        public void CreateDocument()
        {
            try
            {
                if (_kompasObject != null)
                {
                    CloseDocument();
                    if (_ksDocumentObject == null)
                    {
                        _ksDocumentObject = (ksDocument3D)_kompasObject.Document3D();
                        _ksDocumentObject.Create(false, false);
                        _ksDocumentObject = (ksDocument3D)_kompasObject.ActiveDocument3D();
                    }
                }
                else
                {
                    throw new Exception();
                }
            }
            catch (Exception)
            {
                throw new NullReferenceException(@"Сначала откройте KOMPAS 3D");
            }
        }

        public void CloseDocument()
        {
            if (_ksDocumentObject != null)
            {
                _ksDocumentObject.close();
                _ksDocumentObject = null;
            }
        }

        public void CloseKompas3D()
        {
            if (_ksDocumentObject != null)
            {
                CloseDocument();
            }
            try
            {
                _kompasObject.Quit();
            }
            catch
            {
                throw new NullReferenceException();
            }
        }
    }
}
