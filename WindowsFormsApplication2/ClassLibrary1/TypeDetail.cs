﻿namespace ClassLibrary1
{
    // Перечисление типов клавиатуры.
    public enum TypeDetail
    {
        Classic = 0,          // Стандартный тип элемента.
        Alternative           // Альтернативный тип элемента.
    }
}