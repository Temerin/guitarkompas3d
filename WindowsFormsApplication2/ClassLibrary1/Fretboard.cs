﻿namespace ClassLibrary1
{
    // Перечисление материалов грифа.
    public enum Fretboard
    {
        Palisander = 0,         // Палисандер 
        Maple,     			    // Клён
		Redwood					// Красное дерево
    }
}