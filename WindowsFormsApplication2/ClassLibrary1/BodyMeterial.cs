﻿namespace ClassLibrary1
{
    // Перечисление материалов корпуса.
    public enum BodyMaterial
    {
        Cedar = 0,          // Кедар
        Maple,           	// Клён
		Linden,				// Липа
		Pine,				// Сосна
    }
}