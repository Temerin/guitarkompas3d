﻿using System.Drawing;
using Kompas6API5;
using Kompas6Constants3D;

namespace ClassLibrary1
{
    class BodyGuitarBuilder : IBuild
    {
        ksPart part;
        public void Build(ksDocument3D document3D, GuitarDescription data)
        {
            part = (ksPart)document3D.GetPart((short)Part_Type.pTop_Part);
            if (part != null)
            {
                // Построение эскиза корпуса гитары.

                var entitySketch = (ksEntity)part.NewEntity((short)Obj3dType.o3d_sketch);
                if (entitySketch != null)
                {
                    entitySketch.name = "Корпус гитары";

                    var sketchDef = (ksSketchDefinition)entitySketch.GetDefinition();
                    if (sketchDef != null)
                    {
                        var basePlane = (ksEntity)part.GetDefaultEntity((short)Obj3dType.o3d_planeXOY);

                        sketchDef.SetPlane(basePlane);
                        entitySketch.Create();

                        var sketchEdit = (ksDocument2D)sketchDef.BeginEdit();
                
                        sketchEdit.ksNurbs(3, true, 1);
                        sketchEdit.ksPoint(0, 0, 1);
                        sketchEdit.ksPoint(30, 10, 1);
                        sketchEdit.ksPoint(40, 50, 1);
                        sketchEdit.ksPoint(30, 90, 1);
                        sketchEdit.ksPoint(0, 100, 1);
                        sketchEdit.ksPoint(-50, 82, 1);
                        sketchEdit.ksPoint(-80, 90, 1);
                        sketchEdit.ksPoint(-95, 50, 1);
                        sketchEdit.ksPoint(-80, 10, 1);
                        sketchEdit.ksPoint(-50, 18, 1);
                        sketchEdit.ksEndObj();


                        /*sketchEdit.ksLineSeg(0, 0, 0, -data.OutletGuitar, 1);
                        sketchEdit.ksLineSeg(0, -data.OutletGuitar,
                            -data.FretGuitar, -data.OutletGuitar, 1);
                        sketchEdit.ksLineSeg(-data.FretGuitar,
                            -data.OutletGuitar, -data.FretGuitar, 0, 1);
                        sketchEdit.ksLineSeg(-data.FretGuitar, 0, 0, 0, 1);*/

                        sketchDef.EndEdit();
                        BodyExtruseBottom(data, entitySketch);
                    }
                }

                // Эскиз розетки

                entitySketch = (ksEntity)part.NewEntity((short)Obj3dType.o3d_sketch);
                if (entitySketch != null)
                {
                    var sketchDef = (ksSketchDefinition)entitySketch.GetDefinition();
                    if (sketchDef != null)
                    {
                        var basePlane = (ksEntity)part.GetDefaultEntity((short)Obj3dType.o3d_planeXOY);

                        sketchDef.SetPlane(basePlane);
                        entitySketch.Create();

                        var sketchEdit = (ksDocument2D)sketchDef.BeginEdit();

                        if (sketchEdit != null)
                        {

                            sketchEdit.ksCircle(0, 50, data.OutletGuitar, 1);
                        }

                        sketchDef.EndEdit();
                        BodyExtruseTop(data, entitySketch);
                    }
                }
            }
        }


        /// <summary>
        /// Метод выдавливания корпуса гитары.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="entity"></param>
        private void BodyExtruseBottom(GuitarDescription data,
            ksEntity entity)
        {
            var entityExtrusion = (ksEntity)part.NewEntity((short)Obj3dType.o3d_baseExtrusion);
            if (entityExtrusion != null)
            {
                entityExtrusion.name = "Выдавливание тела";

                var extrusionDefinition = (ksBaseExtrusionDefinition)entityExtrusion.GetDefinition();
                if (extrusionDefinition != null)
                {
                    extrusionDefinition.directionType = (short)Direction_Type.dtNormal;
                    extrusionDefinition.SetSideParam(true,
                        (short)End_Type.etBlind, 20);
                    extrusionDefinition.SetThinParam(false, 0, 0, 0);
                    extrusionDefinition.SetSketch(entity);

                    entityExtrusion.SetAdvancedColor(Color.FromArgb(120, 120, 120).ToArgb(),
                        .0, .0, .0, .0, 100, 100);
                    entityExtrusion.Create();
                }
            }
        }

        private void BodyExtruseTop(GuitarDescription data,
            ksEntity entity)
        {
            var entityExtrusion = (ksEntity)part.NewEntity((short)Obj3dType.o3d_cutExtrusion);
            if (entityExtrusion != null)
            {
                entityExtrusion.name = "Выдавливание тела";

                var extrusionDefinition = (ksCutExtrusionDefinition)entityExtrusion.GetDefinition();
                if (extrusionDefinition != null)
                {
                    extrusionDefinition.directionType = (short)Direction_Type.dtReverse;
                    extrusionDefinition.SetSideParam(true,
                        (short)End_Type.etBlind, 17);
                    extrusionDefinition.SetThinParam(false, 0, 0, 0);
                    extrusionDefinition.SetSketch(entity);

                    entityExtrusion.SetAdvancedColor(Color.FromArgb(120, 120, 120).ToArgb(),
                        .0, .0, .0, .0, 100, 100);
                    entityExtrusion.Create();
                }
            }
        }
    }
}
