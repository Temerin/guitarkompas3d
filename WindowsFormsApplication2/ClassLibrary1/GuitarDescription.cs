﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace ClassLibrary1
{
    public class GuitarDescription
    {


        public GuitarDescription() //постоение со стандартными параметрами.
        {
            BridgeGuitar = 0;
            CutGuitar = false;
            FlagGuitar = false;
            FretboardGuitar = 0;
            FretGuitar = 20;
            HeadstockGuitar = 0;
            LowDeckGuitar = 0;
            MarkersGuitar = 0;
            OutletGuitar = 15;
            ShellGuitar = 0;
            TunersGuitar = 0;
            TypeGuitar = 0;
            UpDeckGuitar = 0;
            Validate();
        }

        public bool Record(TypeDetail bridgeGuitar, bool cutGuitar, bool flagGuitar, Fretboard fretboardGuitar, int fretGuitar,
    TypeDetail headstockGuitar, BodyMaterial lowDeckGuitar, Markers markersGuitar, int outletGuitar,
    BodyMaterial shellGuitar, TypeDetail tunersGuitar, TypeDetail typeGuitar, BodyMaterial upDeckGuitar)
        {
            BridgeGuitar = bridgeGuitar;
            CutGuitar = cutGuitar;
            FlagGuitar = flagGuitar;
            FretboardGuitar = fretboardGuitar;
            FretGuitar = fretGuitar;
            HeadstockGuitar = headstockGuitar;
            LowDeckGuitar = lowDeckGuitar;
            MarkersGuitar = markersGuitar;
            OutletGuitar = outletGuitar;
            ShellGuitar = shellGuitar;
            TunersGuitar = tunersGuitar;
            TypeGuitar = typeGuitar;
            UpDeckGuitar = upDeckGuitar;
            return  Validate();
        }
        public TypeDetail BridgeGuitar { get; private set; }        //Тип бриджа
        public bool CutGuitar {get; private set;}                   //Наличие выреза
        public bool FlagGuitar { get; private set; }                //Наличие флажка (защиты от медиатора)
        public Fretboard FretboardGuitar {get; private set;}        //Материал грифа
        public int FretGuitar {get; private set;}                   //Длина грифа
        public TypeDetail HeadstockGuitar { get; private set; }     //Тип головки грифа
        public BodyMaterial LowDeckGuitar {get; private set;}       //Материал нижней деки
        public Markers MarkersGuitar {get; private set;}            //Вид маркеров
        public int OutletGuitar {get; private set;}                 //Размер резонаторного выреза
        public BodyMaterial ShellGuitar {get; private set;}         //Материал оберчайки
        public TypeDetail TunersGuitar {get; private set;}          //Тип колков
        public TypeDetail TypeGuitar {get; private set;}            //Тип гитары
        public BodyMaterial UpDeckGuitar { get; private set; }      //Материал верхней деки

        private bool Validate() //Валидация введенных параметров
        {
            return false;
        }
    }
}
