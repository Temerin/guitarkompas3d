﻿namespace WindowsFormsApplication2
{
    partial class Form1
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.checkBoxCut = new System.Windows.Forms.CheckBox();
            this.comboBoxType = new System.Windows.Forms.ComboBox();
            this.numericFret = new System.Windows.Forms.NumericUpDown();
            this.comboBoxLowDeck = new System.Windows.Forms.ComboBox();
            this.comboBoxUpDeck = new System.Windows.Forms.ComboBox();
            this.comboBoxShell = new System.Windows.Forms.ComboBox();
            this.comboBoxFretboard = new System.Windows.Forms.ComboBox();
            this.comboBoxTuners = new System.Windows.Forms.ComboBox();
            this.comboBoxBridge = new System.Windows.Forms.ComboBox();
            this.comboBoxMarkers = new System.Windows.Forms.ComboBox();
            this.comboBoxHeadstock = new System.Windows.Forms.ComboBox();
            this.checkBoxFlag = new System.Windows.Forms.CheckBox();
            this.numericOutlet = new System.Windows.Forms.NumericUpDown();
            this.labelFret = new System.Windows.Forms.Label();
            this.labelOutlet = new System.Windows.Forms.Label();
            this.panelControl = new System.Windows.Forms.Panel();
            this.buttonBuild = new System.Windows.Forms.Button();
            this.buttonSWStart = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.numericFret)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericOutlet)).BeginInit();
            this.panelControl.SuspendLayout();
            this.SuspendLayout();
            // 
            // checkBoxCut
            // 
            this.checkBoxCut.AutoSize = true;
            this.checkBoxCut.Location = new System.Drawing.Point(151, 16);
            this.checkBoxCut.Name = "checkBoxCut";
            this.checkBoxCut.Size = new System.Drawing.Size(59, 17);
            this.checkBoxCut.TabIndex = 0;
            this.checkBoxCut.Text = "Вырез";
            this.checkBoxCut.UseVisualStyleBackColor = true;
            this.checkBoxCut.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // comboBoxType
            // 
            this.comboBoxType.FormattingEnabled = true;
            this.comboBoxType.Items.AddRange(new object[] {
            "Классика",
            "Вестерн"});
            this.comboBoxType.Location = new System.Drawing.Point(12, 12);
            this.comboBoxType.Name = "comboBoxType";
            this.comboBoxType.Size = new System.Drawing.Size(121, 21);
            this.comboBoxType.TabIndex = 1;
            this.comboBoxType.Text = "Тип гитары";
            // 
            // numericFret
            // 
            this.numericFret.Location = new System.Drawing.Point(174, 57);
            this.numericFret.Maximum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.numericFret.Minimum = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.numericFret.Name = "numericFret";
            this.numericFret.Size = new System.Drawing.Size(35, 20);
            this.numericFret.TabIndex = 2;
            this.numericFret.Value = new decimal(new int[] {
            15,
            0,
            0,
            0});
            // 
            // comboBoxLowDeck
            // 
            this.comboBoxLowDeck.FormattingEnabled = true;
            this.comboBoxLowDeck.Items.AddRange(new object[] {
            "Кедр",
            "Клен",
            "Липа",
            "Сосна"});
            this.comboBoxLowDeck.Location = new System.Drawing.Point(12, 125);
            this.comboBoxLowDeck.Name = "comboBoxLowDeck";
            this.comboBoxLowDeck.Size = new System.Drawing.Size(198, 21);
            this.comboBoxLowDeck.TabIndex = 3;
            this.comboBoxLowDeck.Text = "Материал нижней деки";
            // 
            // comboBoxUpDeck
            // 
            this.comboBoxUpDeck.FormattingEnabled = true;
            this.comboBoxUpDeck.Items.AddRange(new object[] {
            "Кедр",
            "Клен",
            "Липа",
            "Сосна"});
            this.comboBoxUpDeck.Location = new System.Drawing.Point(12, 152);
            this.comboBoxUpDeck.Name = "comboBoxUpDeck";
            this.comboBoxUpDeck.Size = new System.Drawing.Size(198, 21);
            this.comboBoxUpDeck.TabIndex = 4;
            this.comboBoxUpDeck.Text = "Материал верхней деки";
            // 
            // comboBoxShell
            // 
            this.comboBoxShell.FormattingEnabled = true;
            this.comboBoxShell.Items.AddRange(new object[] {
            "Кедр",
            "Клен",
            "Липа",
            "Сосна"});
            this.comboBoxShell.Location = new System.Drawing.Point(12, 179);
            this.comboBoxShell.Name = "comboBoxShell";
            this.comboBoxShell.Size = new System.Drawing.Size(198, 21);
            this.comboBoxShell.TabIndex = 5;
            this.comboBoxShell.Text = "Материал оберчайки";
            // 
            // comboBoxFretboard
            // 
            this.comboBoxFretboard.FormattingEnabled = true;
            this.comboBoxFretboard.Items.AddRange(new object[] {
            "Клен",
            "Красное дерево",
            "Полисандр"});
            this.comboBoxFretboard.Location = new System.Drawing.Point(12, 98);
            this.comboBoxFretboard.Name = "comboBoxFretboard";
            this.comboBoxFretboard.Size = new System.Drawing.Size(197, 21);
            this.comboBoxFretboard.TabIndex = 6;
            this.comboBoxFretboard.Text = "Материал грифа";
            // 
            // comboBoxTuners
            // 
            this.comboBoxTuners.FormattingEnabled = true;
            this.comboBoxTuners.Items.AddRange(new object[] {
            "Классические",
            "Альтернативный вариант"});
            this.comboBoxTuners.Location = new System.Drawing.Point(241, 98);
            this.comboBoxTuners.Name = "comboBoxTuners";
            this.comboBoxTuners.Size = new System.Drawing.Size(186, 21);
            this.comboBoxTuners.TabIndex = 7;
            this.comboBoxTuners.Text = "Форма колков";
            // 
            // comboBoxBridge
            // 
            this.comboBoxBridge.FormattingEnabled = true;
            this.comboBoxBridge.Items.AddRange(new object[] {
            "Классический",
            "С зажимами "});
            this.comboBoxBridge.Location = new System.Drawing.Point(241, 125);
            this.comboBoxBridge.Name = "comboBoxBridge";
            this.comboBoxBridge.Size = new System.Drawing.Size(186, 21);
            this.comboBoxBridge.TabIndex = 8;
            this.comboBoxBridge.Text = "Бридж";
            // 
            // comboBoxMarkers
            // 
            this.comboBoxMarkers.FormattingEnabled = true;
            this.comboBoxMarkers.Items.AddRange(new object[] {
            "Сверху и у струн",
            "Только сверху",
            "Только у струн"});
            this.comboBoxMarkers.Location = new System.Drawing.Point(241, 152);
            this.comboBoxMarkers.Name = "comboBoxMarkers";
            this.comboBoxMarkers.Size = new System.Drawing.Size(186, 21);
            this.comboBoxMarkers.TabIndex = 9;
            this.comboBoxMarkers.Text = "Отметки на ладах";
            // 
            // comboBoxHeadstock
            // 
            this.comboBoxHeadstock.FormattingEnabled = true;
            this.comboBoxHeadstock.Items.AddRange(new object[] {
            "Классическая",
            "Альтернативный вариант"});
            this.comboBoxHeadstock.Location = new System.Drawing.Point(241, 178);
            this.comboBoxHeadstock.Name = "comboBoxHeadstock";
            this.comboBoxHeadstock.Size = new System.Drawing.Size(186, 21);
            this.comboBoxHeadstock.TabIndex = 10;
            this.comboBoxHeadstock.Text = "Форма головки грифа";
            // 
            // checkBoxFlag
            // 
            this.checkBoxFlag.AutoSize = true;
            this.checkBoxFlag.Location = new System.Drawing.Point(241, 16);
            this.checkBoxFlag.Name = "checkBoxFlag";
            this.checkBoxFlag.Size = new System.Drawing.Size(69, 17);
            this.checkBoxFlag.TabIndex = 11;
            this.checkBoxFlag.Text = "Флажок";
            this.checkBoxFlag.UseVisualStyleBackColor = true;
            // 
            // numericOutlet
            // 
            this.numericOutlet.Location = new System.Drawing.Point(392, 57);
            this.numericOutlet.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.numericOutlet.Minimum = new decimal(new int[] {
            7,
            0,
            0,
            0});
            this.numericOutlet.Name = "numericOutlet";
            this.numericOutlet.Size = new System.Drawing.Size(35, 20);
            this.numericOutlet.TabIndex = 12;
            this.numericOutlet.ThousandsSeparator = true;
            this.numericOutlet.Value = new decimal(new int[] {
            7,
            0,
            0,
            0});
            // 
            // labelFret
            // 
            this.labelFret.AutoSize = true;
            this.labelFret.Location = new System.Drawing.Point(9, 64);
            this.labelFret.Name = "labelFret";
            this.labelFret.Size = new System.Drawing.Size(99, 13);
            this.labelFret.TabIndex = 13;
            this.labelFret.Text = "Количество ладов";
            // 
            // labelOutlet
            // 
            this.labelOutlet.AutoSize = true;
            this.labelOutlet.Location = new System.Drawing.Point(238, 64);
            this.labelOutlet.Name = "labelOutlet";
            this.labelOutlet.Size = new System.Drawing.Size(97, 13);
            this.labelOutlet.TabIndex = 14;
            this.labelOutlet.Text = "Диаметр розетки";
            // 
            // panelControl
            // 
            this.panelControl.Controls.Add(this.buttonSWStart);
            this.panelControl.Controls.Add(this.buttonBuild);
            this.panelControl.Location = new System.Drawing.Point(12, 238);
            this.panelControl.Name = "panelControl";
            this.panelControl.Size = new System.Drawing.Size(170, 87);
            this.panelControl.TabIndex = 15;
            // 
            // buttonBuild
            // 
            this.buttonBuild.Location = new System.Drawing.Point(32, 45);
            this.buttonBuild.Name = "buttonBuild";
            this.buttonBuild.Size = new System.Drawing.Size(109, 23);
            this.buttonBuild.TabIndex = 0;
            this.buttonBuild.Text = "Постоить модель";
            this.buttonBuild.UseVisualStyleBackColor = true;
            // 
            // buttonSWStart
            // 
            this.buttonSWStart.Location = new System.Drawing.Point(32, 16);
            this.buttonSWStart.Name = "buttonSWStart";
            this.buttonSWStart.Size = new System.Drawing.Size(109, 23);
            this.buttonSWStart.TabIndex = 1;
            this.buttonSWStart.Text = "Запуск SolidWorks";
            this.buttonSWStart.UseVisualStyleBackColor = true;
            this.buttonSWStart.Click += new System.EventHandler(this.buttonSWStart_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 238);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 13);
            this.label1.TabIndex = 16;
            this.label1.Text = "Управление";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(439, 366);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panelControl);
            this.Controls.Add(this.labelOutlet);
            this.Controls.Add(this.labelFret);
            this.Controls.Add(this.numericOutlet);
            this.Controls.Add(this.checkBoxFlag);
            this.Controls.Add(this.comboBoxHeadstock);
            this.Controls.Add(this.comboBoxMarkers);
            this.Controls.Add(this.comboBoxBridge);
            this.Controls.Add(this.comboBoxTuners);
            this.Controls.Add(this.comboBoxFretboard);
            this.Controls.Add(this.comboBoxShell);
            this.Controls.Add(this.comboBoxUpDeck);
            this.Controls.Add(this.comboBoxLowDeck);
            this.Controls.Add(this.numericFret);
            this.Controls.Add(this.comboBoxType);
            this.Controls.Add(this.checkBoxCut);
            this.Name = "Form1";
            this.Text = "Конструктор гитары";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numericFret)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericOutlet)).EndInit();
            this.panelControl.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox checkBoxCut;
        private System.Windows.Forms.ComboBox comboBoxType;
        private System.Windows.Forms.NumericUpDown numericFret;
        private System.Windows.Forms.ComboBox comboBoxLowDeck;
        private System.Windows.Forms.ComboBox comboBoxUpDeck;
        private System.Windows.Forms.ComboBox comboBoxShell;
        private System.Windows.Forms.ComboBox comboBoxFretboard;
        private System.Windows.Forms.ComboBox comboBoxTuners;
        private System.Windows.Forms.ComboBox comboBoxBridge;
        private System.Windows.Forms.ComboBox comboBoxMarkers;
        private System.Windows.Forms.ComboBox comboBoxHeadstock;
        private System.Windows.Forms.CheckBox checkBoxFlag;
        private System.Windows.Forms.NumericUpDown numericOutlet;
        private System.Windows.Forms.Label labelFret;
        private System.Windows.Forms.Label labelOutlet;
        private System.Windows.Forms.Panel panelControl;
        private System.Windows.Forms.Button buttonSWStart;
        private System.Windows.Forms.Button buttonBuild;
        private System.Windows.Forms.Label label1;
    }
}

